//
//  main.m
//  metotlar
//
//  Created by Yakup on 14.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <math.h>


@interface matematik :NSObject
{
    // Değişkenleri tanımlıyoruz.
    int enKucuk;
}
//Metorları deklere ediyoruz.
-(void) geriDonüssüzMetot;
-(double)kareKok:(int)sayi;
-(int)min:(int)sayi1 ve:(int)sayi2;
@end

@implementation matematik
// Sınıfları gerçekleştiriyoruz.
-(void) geriDonüssüzMetot{
    NSLog(@"\nBen geri dönüş değeri olmayan bir metodum!...");
}

-(double)kareKok:(int)sayi{
    return sqrt(sayi);
}

-(int)min:(int)sayi1 ve:(int)sayi2{
    if (sayi1<sayi2)
        enKucuk = sayi1;
    else
        enKucuk = sayi2;
    
return enKucuk;
}


@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        int sayi,sayi2;
        matematik *nesne = [[matematik alloc]init]; // metematik sınıfından bir nesne oluşturuyoruz.
        [nesne geriDonüssüzMetot];
        
        NSLog(@"Bir Sayi Gir: "); scanf("%i",&sayi);
        NSLog(@"\nGirilen iki sayinin Karekökü: %f \n",[nesne kareKok:sayi]);
        
        NSLog(@"Birinci Sayi Gir: "); scanf("%i",&sayi);
        NSLog(@"İkinci Sayi Gir: "); scanf("%i",&sayi2);
        NSLog(@"Girilen iki sayinin kücüğü: %d ",[nesne min:3 ve:9]);

       

        
        
        
        
    }
    return 0;
}
